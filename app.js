const request             = require("request")
const fs                  = require('fs')
const membersIdFile       = './membersIdFile.json'
const eventsIdFile        = './eventsIdFile.json'
const getInfo             = require('./modules/getInfo')
const URLMen              = 'https://www.sofascore.com/tennis/itf-men//json'
const URLWomen            = 'https://www.sofascore.com/tennis/itf-women//json'
const TelegramBot         = require('node-telegram-bot-api')
const token               = 'ключ от бота'
const bot                 = new TelegramBot(token, {polling: true})
let members               = [];
let events                = [];

if (fs.existsSync(membersIdFile)) {
  fs.readFile(membersIdFile, "utf8", (err, dataMembers) => {
    members = JSON.parse(dataMembers)
  })
} else {
  membersFile = JSON.stringify(members);
  fs.writeFile(membersIdFile, membersFile, (err) => {
    if (err) console.error(err)
  })
}

if (fs.existsSync(eventsIdFile)) {
  fs.readFile(eventsIdFile, "utf8", (err, dataEvents) => {
    events = JSON.parse(dataEvents)
  })
} else {
  eventsFile = JSON.stringify(events);
  fs.writeFile(eventsIdFile, eventsFile, (err) => {
    if (err) console.error(err)
  })
}

bot.on('message', (msg) => {
  const chatId = msg.chat.id
  if(members.includes(chatId) === false) {
    members.push(chatId)
    membersFile = JSON.stringify(members);
    fs.writeFile(membersIdFile, membersFile, (err) => {
      if (err) console.error(err)
      bot.sendMessage(chatId, 'Вы успешно авторизовались')
    })
  } else bot.sendMessage(chatId, 'Вы уже авторизированы')
})

setInterval(() => {
  getInfo(URLMen, (data) => {
    let id          = data["id"];
    if(events.includes(id) === false) {
      events.push(id)
      eventsFile = JSON.stringify(events)
      fs.writeFileSync(eventsIdFile, eventsFile)
      for (var i = 0; i < Object.keys(members).length; i++) {
        bot.sendMessage(members[i], `--- ${data["category"]}\n-- ${data["tournament"]}\n ${data["homeTeamName"]} - ${data["awayTeamName"]}\n СЧЕТ ПЕРВОГО СЕТА: ${data["score"]}`)
      }
    }
  })
  getInfo(URLWomen, (data) => {
    let id          = data["id"];
    if(events.includes(id) === false) {
      events.push(id)
      eventsFile = JSON.stringify(events)
      fs.writeFileSync(eventsIdFile, eventsFile)
      for (var i = 0; i < Object.keys(members).length; i++) {
        bot.sendMessage(members[i], `--- ${data["category"]}\n-- ${data["tournament"]}\n ${data["homeTeamName"]} - ${data["awayTeamName"]}\n СЧЕТ ПЕРВОГО СЕТА: ${data["score"]}`)
      }
    }
  })
}, 60 * 1000);
