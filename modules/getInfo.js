const request   = require("request")

module.exports  = (url, data) => {
  request({
    url: url,
    json: true
  }, (error, response, body) => {
    if (!error && response.statusCode === 200) {
      let date              = new Date;
      console.log(`===== ${date} `);
      let tournaments = body["sportItem"]["tournaments"]
      for (var j = 0; j < Object.keys(tournaments).length; j++) {
        let events  = tournaments[j]["events"]
        for (var i  = 0; i < Object.keys(events).length; i++) {
        if(events[i]['status']['type'] == "inprogress") {
            let event             = events[i];
            let id                = event["id"]
            let categoryName      = tournaments[j]["category"]["name"]
            let tournamentName    = tournaments[j]["tournament"]["name"]
            let homeTeamName      = event["homeTeam"]["name"]
            let awayTeamName      = event["awayTeam"]["name"]
            let statusDescription = event["statusDescription"]
            let score             = `${event["homeScore"]["period1"]}-${event["awayScore"]["period1"]}`
            console.log(`| --- ${categoryName} | ${tournamentName} | ${homeTeamName} - ${awayTeamName} | (${score})`);
            if(statusDescription != "1. set"){
              switch(score) {
                case '6-0':
                case '6-1':
                case '6-2':
                case '6-3':
                case '0-6':
                case '1-6':
                case '2-6':
                case '3-6':
                  data({
                    id : id,
                    category : categoryName,
                    tournament : tournamentName,
                    homeTeamName : homeTeamName,
                    awayTeamName : awayTeamName,
                    score : score
                  })
                  break;
                default:
                  break;
              }
            }
          }
        }
      }
      console.log(`===== `);
    }
  })
}
